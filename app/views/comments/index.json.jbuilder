json.array!(@comments) do |comment|
  json.extract! comment, :id, :auther, :text
  json.url comment_url(comment, format: :json)
end
